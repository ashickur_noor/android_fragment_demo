package com.example.ashickurrahman.fragment1;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements MyListner {

    Button bAddA,bAddb,bRemA,bRemB,bRplcA,bRplcB;
    FragmentManager manager;
    FragmentTransaction transaction;
    FragmentA fragmentA;
    FragmentB fragmentB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bAddA=findViewById(R.id.fragaaddbtn);
        bAddb=findViewById(R.id.fragbaddbtn);
        bRemA=findViewById(R.id.fragarembtn);
        bRemB=findViewById(R.id.fragbrembtn);
        bRplcA=findViewById(R.id.fragrplcabtn);
        bRplcB=findViewById(R.id.fragrplcbbtn);

        manager= getFragmentManager();

        bAddA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentA = new FragmentA();
                Bundle bundle = new Bundle();
                bundle.putString("hello","Set From Activity");
                fragmentA.setArguments(bundle);
                transaction=manager.beginTransaction();
                transaction.add(R.id.fragholder1,fragmentA,"fragA");
                transaction.commit();
            }
        });
        bRemA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragmentA = new FragmentA();
                transaction=manager.beginTransaction();
                transaction.remove(fragmentA);
                transaction.commit();
            }
        });

        bAddb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentB = new FragmentB();
                transaction=manager.beginTransaction();
                transaction.add(R.id.fragholder2,fragmentB,"fragb");
                transaction.commit();
            }
        });

        bRemB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //fragmentB = new FragmentB();
                transaction=manager.beginTransaction();
                transaction.remove(fragmentB);
                transaction.commit();
            }
        });

        bRplcA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentB = new FragmentB();
                transaction=manager.beginTransaction();
                transaction.replace(R.id.fragholder1,fragmentB);
                transaction.commit();
            }
        });

        bRplcB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentA = new FragmentA();
                transaction=manager.beginTransaction();
                transaction.replace(R.id.fragholder2,fragmentA);
                transaction.commit();
            }
        });


    }

    @Override
    public void setData(String data) {
        FragmentB fragmentB = (FragmentB) manager.findFragmentByTag("fragb");
        fragmentB.setDatainFragb(data);
    }
}

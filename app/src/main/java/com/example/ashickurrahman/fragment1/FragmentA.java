package com.example.ashickurrahman.fragment1;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment {

    EditText aEditText;
    TextView textView;
    public FragmentA() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row=inflater.inflate(R.layout.fragment_a, container, false);

        textView=row.findViewById(R.id.tv1);
        aEditText=row.findViewById(R.id.fragmentedittext);
        Button button=row.findViewById(R.id.fragmentbutton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyListner listner = (MyListner) getActivity();
                listner.setData(aEditText.getText().toString());
            }
        });
        Bundle bundle=getArguments();
        String stringText = bundle.getString("hello");
        textView.setText(stringText);
        return row;
    }

}

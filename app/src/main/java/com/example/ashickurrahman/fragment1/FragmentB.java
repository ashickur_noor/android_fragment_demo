package com.example.ashickurrahman.fragment1;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {


    public FragmentB() {
        // Required empty public constructor
    }

    TextView tv2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View row=inflater.inflate(R.layout.fragment_b, container, false);
        tv2=row.findViewById(R.id.tv2);
        return row;
    }

    public void  setDatainFragb(String data){
        tv2.setText(data);
    }

}
